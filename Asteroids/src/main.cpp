#define SDL_MAIN_HANDLED
#include "Game.h"
#include "TimeKeeper.h"

Game* game = nullptr;

int main(int argc, char* args[])
{
	game = new Game("Asteroids", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 640, false);
	game->Initialise();

	srand(time(NULL));	// This seeds the random numbers so it isn't the same value each time

	while (game->IsRunning())
	{
		game->HandleEvents();
		game->Update();
		game->Render();

		TimeKeeper::CalculateDeltaTime();
	}
	
	delete game;
	return 0;
}