#include "Texture.h"
#include "Game.h"
#include <iostream>

Texture::Texture(const char* texturePath)
{
  SDL_Surface* surface = LoadSurface(texturePath);
  CreateTexture(surface, texturePath);
  SDL_FreeSurface(surface);
}

Texture::~Texture()
{
  SDL_DestroyTexture(texture);
}

SDL_Surface* Texture::LoadSurface(const char* texturePath)
{
  SDL_Surface* surface = IMG_Load(texturePath);
  if (!surface)
  {
    std::cout << "ERROR: Unable to load image: " << texturePath << '\n';
    return NULL;
  }
  return surface;
}

void Texture::CreateTexture(SDL_Surface* surface, const char* texturePath)
{
  texture = SDL_CreateTextureFromSurface(Game::GET_RENDERER(), surface);
  if (!texture)
  {
    std::cout << "ERROR: Unable to create texture from surface: " << texturePath << '\n';
  }
}