#include "TimeKeeper.h"

double TimeKeeper::deltaTime = 0;
Uint64 TimeKeeper::ticksPreviousFrame = 0;
Uint64 TimeKeeper::ticksThisFrame = 0;

void TimeKeeper::CalculateDeltaTime()
{
  ticksPreviousFrame = ticksThisFrame;
  ticksThisFrame = SDL_GetPerformanceCounter();

  if (ticksPreviousFrame == 0) deltaTime = 0;
  else deltaTime = (double)(ticksThisFrame - ticksPreviousFrame) / SDL_GetPerformanceFrequency();
}