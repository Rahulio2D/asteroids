#include "Point.h"

Point::Point()
{
	x = y = 0;
}

Point::Point(float x, float y)
{
	this->x = x;
	this->y = y;
}

std::ostream& operator<<(std::ostream& os, Point p)
{
	return os << "(" << p.x << ", " << p.y << ")";
}