#include "GameObjects/AsteroidSpawner.h"
#include "TimeKeeper.h"
#include "Game.h"

AsteroidSpawner::AsteroidSpawner(Player* player)
{
  this->player = player;
  asteroids = std::vector<Asteroid*>();
  screenSizeInUnits = Vector2(Game::WIDTH, Game::HEIGHT) / Camera::GetUnitSize();
}

AsteroidSpawner::~AsteroidSpawner()
{
  player = nullptr;
  for (auto asteroid : asteroids) delete asteroid;
  asteroids.clear();
}

void AsteroidSpawner::Initialise()
{
  StopAndClear();

  for (int i = 0; i < 3; i++)
  {
    asteroids.push_back(SpawnAsteroid());
  }
  timeToNextAsteroidSpawn = maxTimeToSpawnAsteroid;
}

void AsteroidSpawner::Update()
{
  timeToNextAsteroidSpawn -= TimeKeeper::GetDeltaTime();
  if (timeToNextAsteroidSpawn <= 0)
  {
    timeToNextAsteroidSpawn = maxTimeToSpawnAsteroid;
    asteroids.push_back(SpawnAsteroid());
  }

  for (int i = 0; i < asteroids.size(); i++)
  {
    asteroids[i]->Update();
    player->CheckCollisionWithAsteroid(asteroids[i]);
    if (asteroids[i]->IsDestroyed())
    {
      if (!asteroids[i]->IsMiniAsteroid())
      {
        for (int j = 0; j < 2; j++)
        {
          // Create two mini asteroids
          asteroids.push_back(new Asteroid(asteroids[i]->transform->position, player->transform->position, true));
        }
      }

      delete asteroids[i];
      asteroids.erase(asteroids.begin() + i);
    }
  }
}

Asteroid* AsteroidSpawner::SpawnAsteroid()
{
  Vector2 spawnPosition;
  if (rand() % 2 == 0)
  {
    // Spawn on left or right side of window
    spawnPosition.x = (rand() % 2 == 0) ? screenSizeInUnits.x / 2 : -screenSizeInUnits.x / 2;
    spawnPosition.y = (screenSizeInUnits.y / 2) - (screenSizeInUnits.y * ((rand() % 100) / 100.0f));
  }
  else
  {
    // Spawn on top or bottom side of window
    spawnPosition.x = (screenSizeInUnits.x / 2) - (screenSizeInUnits.x * ((rand() % 100) / 100.0f));
    spawnPosition.y = (rand() % 2 == 0) ? screenSizeInUnits.y / 2 : -screenSizeInUnits.y / 2;
  }
  return new Asteroid(spawnPosition, player->transform->position, false);
}

void AsteroidSpawner::StopAndClear()
{
  for (auto asteroid : asteroids) delete asteroid;
  asteroids.clear();
  timeToNextAsteroidSpawn = maxTimeToSpawnAsteroid;
}

void AsteroidSpawner::Render()
{
  for (int i = 0; i < asteroids.size(); i++)
  {
    asteroids[i]->Render();
  }
}
