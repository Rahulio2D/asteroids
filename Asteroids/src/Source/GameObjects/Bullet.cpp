#include "GameObjects/Bullet.h"
#include "TimeKeeper.h"
#include "Game.h"

Bullet::Bullet(Vector2 spawnPosition, float bulletAngle)
{
  transform->position = spawnPosition;
  transform->size = Vector2(0.2f, 0.8f);
  transform->rotation = bulletAngle;

  SDL_Rect sourceRect{ 512, 1536, 512, 512 };
  textureRenderer = new TextureRenderer(Game::GET_MAIN_TEXTURE(), transform, sourceRect);
  boxCollider = new BoxCollider(transform);

  float angleInRadians = bulletAngle * (M_PI / 180.0f);
  moveDirection = Vector2(sin(angleInRadians), cos(angleInRadians));
}

Bullet::~Bullet()
{
  delete textureRenderer;
  delete boxCollider;
}

void Bullet::Initialise()
{

}

void Bullet::Update()
{
  transform->position += moveDirection * moveSpeed * TimeKeeper::GetDeltaTime();

  timeAlive += TimeKeeper::GetDeltaTime();
}

void Bullet::Render()
{
  textureRenderer->Render();
}
