#include "GameObjects/Asteroid.h"
#include "TimeKeeper.h"
#include "Game.h"

Asteroid::Asteroid(Vector2 spawnPosition, Vector2 playerPosition, bool isMiniAsteroid)
{
  SDL_Rect sourceRect{ 512, 1024, 512, 512 };
  textureRenderer = new TextureRenderer(Game::GET_MAIN_TEXTURE(), transform, sourceRect);
  boxCollider = new BoxCollider(transform, Vector2(), Vector2(0.7, 0.7));

  screenSizeInUnits = Vector2(Game::WIDTH, Game::HEIGHT) / Camera::GetUnitSize();

  transform->position = spawnPosition;
  this->isMiniAsteroid = isMiniAsteroid;
  if(!isMiniAsteroid) transform->size = Vector2(2.3, 2.3);

  moveSpeed = ((rand() % 100) / 100.0f) + 2;
  rotationSpeed = rand() % 45 + 45;

  if (isMiniAsteroid) moveDirection = Vector2(((rand() % 200) - 100) / 100.0f, ((rand() % 200) - 100) / 100.0f);
  else
  {
    moveDirection = (playerPosition - transform->position);
    moveDirection.Normalise();
  }
}

Asteroid::~Asteroid()
{
  delete textureRenderer;
  delete boxCollider;
}

void Asteroid::Initialise()
{

}

void Asteroid::Update()
{
  UpdatePosition();
  Rotate();
  KeepOnScreen();
}

void Asteroid::Render()
{
  textureRenderer->Render();
}

void Asteroid::UpdatePosition()
{
  transform->position += moveDirection * moveSpeed * TimeKeeper::GetDeltaTime();
}

void Asteroid::Rotate()
{
  transform->rotation += rotationSpeed * TimeKeeper::GetDeltaTime();
}

void Asteroid::Destroy()
{
  isDestroyed = true;
}

void Asteroid::KeepOnScreen()
{
  if (transform->position.x >= screenSizeInUnits.x / 2) transform->position.x = -screenSizeInUnits.x / 2;
  else if (transform->position.x <= -screenSizeInUnits.x / 2) transform->position.x = screenSizeInUnits.x / 2;

  if (transform->position.y >= screenSizeInUnits.y / 2) transform->position.y = -screenSizeInUnits.y / 2;
  else if (transform->position.y <= -screenSizeInUnits.y / 2) transform->position.y = screenSizeInUnits.y / 2;
}
