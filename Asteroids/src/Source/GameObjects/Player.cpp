#include "GameObjects/Player.h"
#include "TimeKeeper.h"
#include "Game.h"

Player::Player()
{
  SDL_Rect sourceRect{ 0, 1024, 512, 512 };
  textureRenderer = new TextureRenderer(Game::GET_MAIN_TEXTURE(), transform, sourceRect);

  transform->position = Vector2(0, 2.0f);
  boxCollider = new BoxCollider(transform, Vector2(), Vector2(0.5, 0.5));

  screenSizeInUnits = Vector2(Game::WIDTH, Game::HEIGHT) / Camera::GetUnitSize();
  bullets = std::vector<Bullet*>();
}

Player::~Player()
{
  delete textureRenderer;
  delete boxCollider;
  for (auto bullet : bullets) delete bullet;
  bullets.clear();
}

void Player::Initialise()
{
  
}

void Player::Update()
{
  if (Input::GetKey(SDLK_w) || Input::GetKey(SDLK_UP)) Accelerate();
  else Decelerate();

  if (Input::GetKey(SDLK_a) || Input::GetKey(SDLK_LEFT)) Turn(-1);
  else if(Input::GetKey(SDLK_d) || Input::GetKey(SDLK_RIGHT)) Turn(1);

  if (Input::GetKeyDown(SDLK_SPACE)) ShootBullet();

  UpdatePosition();
  UpdateBullets();
}

void Player::Accelerate()
{
  hasStartedMoving = true;

  currentAccelerationAngle = transform->rotation;
  currentAccelerationSpeed += (maxAccelerationSpeed / timeToMaxAcceleration) * TimeKeeper::GetDeltaTime();
  if (currentAccelerationSpeed > maxAccelerationSpeed) currentAccelerationSpeed = maxAccelerationSpeed;
}

void Player::Decelerate()
{
  if (!hasStartedMoving) return;

  currentAccelerationSpeed -= (maxAccelerationSpeed / 2.0f) * TimeKeeper::GetDeltaTime();
  if (currentAccelerationSpeed < maxFloatingSpeed) currentAccelerationSpeed = maxFloatingSpeed;
}

void Player::Turn(int direction) 
{
  transform->rotation += rotationSpeed * direction * (float)TimeKeeper::GetDeltaTime();
}

void Player::UpdatePosition()
{
  float angleInRadians = currentAccelerationAngle * (M_PI / 180.0f);
  Vector2 direction = Vector2(sin(angleInRadians), cos(angleInRadians));
  transform->position += direction * currentAccelerationSpeed * TimeKeeper::GetDeltaTime();

  KeepOnScreen();
}

void Player::ShootBullet()
{
  bullets.push_back(new Bullet(transform->position, transform->rotation));
}

void Player::UpdateBullets()
{
  for (int i = 0; i < bullets.size(); i++)
  {
    if (bullets[i]->IsDestroyed())
    {
      delete bullets[i];
      bullets.erase(bullets.begin() + i);
      continue;
    }
    bullets[i]->Update();
  }
}

void Player::CheckCollisionWithAsteroid(Asteroid* asteroid)
{
  BoxCollider* asteroidCollider = asteroid->GetBoxCollider();

  if (boxCollider->HasCollided(asteroidCollider))
  {
    Game::GameOver();
  }

  for (int i = 0; i < bullets.size(); i++)
  {
    if (bullets[i]->GetBoxCollider()->HasCollided(asteroidCollider))
    {
      delete bullets[i];
      bullets.erase(bullets.begin() + i);
      asteroid->Destroy();
    }
  }
}


void Player::KeepOnScreen()
{
  if (transform->position.x >= screenSizeInUnits.x / 2) transform->position.x = -screenSizeInUnits.x / 2;
  else if (transform->position.x <= -screenSizeInUnits.x / 2) transform->position.x = screenSizeInUnits.x / 2;

  if (transform->position.y >= screenSizeInUnits.y / 2) transform->position.y = -screenSizeInUnits.y / 2;
  else if (transform->position.y <= -screenSizeInUnits.y / 2) transform->position.y = screenSizeInUnits.y / 2;
}

void Player::Render()
{
  textureRenderer->Render();

  for (int i = 0; i < bullets.size(); i++)
    bullets[i]->Render();
}
