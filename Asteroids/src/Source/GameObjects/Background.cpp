#include "GameObjects/Background.h"
#include "Game.h"

Background::Background()
{
  SDL_Rect sourceRect{ 1024, 0, 1024, 1024 };
  textureRenderer = new TextureRenderer(Game::GET_MAIN_TEXTURE(), transform, sourceRect);
  transform->size = Vector2(Game::WIDTH, Game::HEIGHT) / Camera::GetUnitSize();
}

Background::~Background()
{
  delete textureRenderer;
}

void Background::Initialise()
{

}

void Background::Update()
{

}

void Background::Render()
{
  textureRenderer->Render();
}

void Background::SetIsMainMenu(bool isMainMenu)
{
  this->isMainMenu = isMainMenu;
  if (isMainMenu) textureRenderer->SetSourceRect(SDL_Rect{ 1024, 0, 1024, 1024 });
  else textureRenderer->SetSourceRect(SDL_Rect{ 0, 0, 1024, 1024 });
}
