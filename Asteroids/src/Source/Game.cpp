#include "Game.h"
#include <iostream>

SDL_Renderer* Game::renderer = nullptr;
Texture* Game::mainTexture = nullptr;
unsigned int Game::WIDTH = 0;
unsigned int Game::HEIGHT = 0;
bool Game::isMainMenu = true;

Game::Game(const char* title, int xPos, int yPos, int width, int height, bool fullscreen)
{
	window = nullptr;
	camera = nullptr;
	input = nullptr;
	player = nullptr;
	asteroidSpawner = nullptr;
	background = nullptr;
	
	if (InitialiseSDL(title, xPos, yPos, width, height, fullscreen) == 0)
		isRunning = true;

	WIDTH = width;
	HEIGHT = height;
}

Game::~Game()
{
	delete camera;
	delete input;
	delete player;
	delete asteroidSpawner;
	delete background;
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	SDL_Quit();
}

void Game::Initialise()
{
	camera = new Camera(12);
	input = new Input();
	mainTexture = new Texture("Assets/Textures/MainTexture.png");
	player = new Player();
	background = new Background();
	asteroidSpawner = new AsteroidSpawner(player);
}

int Game::CreateRenderer()
{
	renderer = SDL_CreateRenderer(window, -1, 0);
	if (!renderer)
	{
		std::cout << "ERROR: Unable to create renderer!\n";
		return -1;
	}
	return 0;
}

void Game::Update()
{
	if (isMainMenu)
	{
		asteroidSpawner->StopAndClear();
		background->SetIsMainMenu(true);
		if (Input::GetKeyDown(SDLK_ESCAPE)) isRunning = false;
		if (Input::GetKeyDown(SDLK_SPACE))
		{
			asteroidSpawner->Initialise();
			background->SetIsMainMenu(false);
			isMainMenu = false;
		}
	} 
	else
	{
		asteroidSpawner->Update();
		if (Input::GetKeyDown(SDLK_ESCAPE)) isMainMenu = true;
	}
	player->Update();
}

void Game::HandleEvents()
{
	input->ClearInput();

	SDL_Event event;
	SDL_PollEvent(&event);

	switch (event.type)
	{
	case SDL_KEYDOWN:
		input->AddKeyPressed(event.key.keysym.sym);
		break;
	case SDL_KEYUP:
		input->AddKeyReleased(event.key.keysym.sym);
		break;
	case SDL_QUIT:
		isRunning = false;
		break;
	default:
		break;
	}
}

void Game::Render()
{
	SDL_SetRenderDrawColor(renderer, 12, 12, 12, 255);
	SDL_RenderClear(renderer);

	// Add render logic here
	background->Render();
	player->Render();
	asteroidSpawner->Render();

	SDL_RenderPresent(renderer);
}

void Game::GameOver()
{
	isMainMenu = true;
}

int Game::InitialiseSDL(const char* title, int xPos, int yPos, int width, int height, bool fullscreen)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		CreateWindow(title, xPos, yPos, width, height, fullscreen);
		CreateRenderer();
		if (window && renderer) return 0;
	}
	return -1;
}

int Game::CreateWindow(const char* title, int xPos, int yPos, int width, int height, bool fullscreen)
{
	window = SDL_CreateWindow(title, xPos, yPos, width, height, fullscreen);
	if (!window)
	{
		std::cout << "ERROR: Unable to create window!\n";
		return -1;
	}
	return 0;
}