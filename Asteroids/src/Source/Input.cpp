#include "Input.h"

std::vector<SDL_Keycode> Input::keysCurrentlyHeld;
std::vector<SDL_Keycode> Input::keysPressedThisFrame;
std::vector<SDL_Keycode> Input::keysReleasedThisFrame;

Input::Input()
{
  keysCurrentlyHeld = std::vector<SDL_Keycode>();
  keysPressedThisFrame = std::vector<SDL_Keycode>();
  keysReleasedThisFrame = std::vector<SDL_Keycode>();
}

Input::~Input() 
{
  keysCurrentlyHeld.clear();
  keysPressedThisFrame.clear();
  keysReleasedThisFrame.clear();
}

void Input::ClearInput()
{
  keysPressedThisFrame.clear();
  keysReleasedThisFrame.clear();
}

void Input::AddKeyPressed(SDL_Keycode key)
{
  if(!FoundKey(&keysCurrentlyHeld, key) && AddUniqueKey(&keysPressedThisFrame, key) != -1)
    keysCurrentlyHeld.push_back(key);
}

void Input::AddKeyReleased(SDL_Keycode key)
{
  if (AddUniqueKey(&keysReleasedThisFrame, key) != -1)
  {
    for (int i = 0; i < keysCurrentlyHeld.size(); i++)
    {
      if (keysCurrentlyHeld[i] == key)
        keysCurrentlyHeld.erase(keysCurrentlyHeld.begin() + i);
    }
  }
}

bool Input::GetKey(SDL_Keycode key)
{
  return FoundKey(&keysCurrentlyHeld, key);
}

bool Input::GetKeyDown(SDL_Keycode key)
{
  return FoundKey(&keysPressedThisFrame, key);
}

bool Input::GetKeyUp(SDL_Keycode key)
{
  return FoundKey(&keysReleasedThisFrame, key);
}

int Input::AddUniqueKey(std::vector<SDL_Keycode>* keysVector, SDL_Keycode key)
{
  if (FoundKey(keysVector, key)) return -1;

  keysVector->push_back(key);
  return 0;
}

bool Input::FoundKey(std::vector<SDL_Keycode>* keysVector, SDL_Keycode key)
{
  for (int i = 0; i < keysVector->size(); i++)
    if (keysVector->at(i) == key) return true;
  return false;
}
