#include "BoxCollider.h"

BoxCollider::BoxCollider(Transform* transform, Vector2 offset, Vector2 size)
{
  this->transform = transform;
  this->offset = offset;
  this->size = size;
}

bool BoxCollider::HasCollided(BoxCollider* otherCollider)
{
  Vector2 thisBoxPosition = GetPosition();
  Vector2 thisBoxSize = GetSize();
  Vector2 otherBoxPosition = otherCollider->GetPosition();
  Vector2 otherBoxSize = otherCollider->GetSize();

  if (thisBoxPosition.x - thisBoxSize.x / 2 > otherBoxPosition.x + otherBoxSize.x / 2) return false;
  if (thisBoxPosition.x + thisBoxSize.x / 2 < otherBoxPosition.x - otherBoxSize.x / 2) return false;
  if (thisBoxPosition.y - thisBoxSize.y / 2 > otherBoxPosition.y + otherBoxSize.y / 2) return false;
  if (thisBoxPosition.y + thisBoxSize.y / 2 < otherBoxPosition.y - otherBoxSize.y / 2) return false;

  return true;
}

Vector2 BoxCollider::GetPosition()
{
  return transform->position + offset;
}

Vector2 BoxCollider::GetSize()
{
  return transform->size * size;
}
