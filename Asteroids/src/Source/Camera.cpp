#include "Camera.h"
#include "Game.h"

float Camera::orthographicSize = 0;
float Camera::unitSize = 0;
Point Camera::center;

Camera::Camera(float orthographicSize = 5)
{
  center = Point(Game::WIDTH / 2, Game::HEIGHT / 2);
  SetOrthographicSize(orthographicSize);
}

void Camera::SetOrthographicSize(float orthographicSize)
{
  this->orthographicSize = orthographicSize;
  unitSize = Game::HEIGHT / (orthographicSize * 2);
}

SDL_Rect Camera::GetDestinationRect(Vector2 position, Vector2 size)
{
  position *= unitSize;
  size *= unitSize;
  return
  {
    center.x - (int)(size.x / 2) + (int)position.x,
    center.y - (int)(size.y / 2) - (int)position.y,
    (int)(size.x),
    (int)(size.y)
  };
}