#include "TextureRenderer.h"
#include "Camera.h"
#include "Game.h"

TextureRenderer::TextureRenderer(Texture* texture, Transform* transform, SDL_Rect sourceRect)
{
  this->texture = texture;
  this->transform = transform;
  this->sourceRect = sourceRect;
  destinationRect = { 0, 0, 0, 0 };
}

TextureRenderer::~TextureRenderer()
{
  texture = nullptr;
  transform = nullptr;
}

void TextureRenderer::SetSourceRect(SDL_Rect sourceRect)
{
  this->sourceRect = sourceRect;
}

void TextureRenderer::CalculateDestinationRect()
{
  destinationRect = Camera::GetDestinationRect(transform->position, transform->size);
}

void TextureRenderer::Render()
{
  CalculateDestinationRect();
  SDL_RenderCopyEx(
    Game::GET_RENDERER(),
    texture->GetTexture(),
    &sourceRect,
    &destinationRect,
    transform->rotation,
    NULL,
    SDL_FLIP_NONE
  );
}