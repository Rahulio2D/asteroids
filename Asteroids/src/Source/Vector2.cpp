#include "Vector2.h"

Vector2::Vector2()
{
	x = y = 0;
}

Vector2::Vector2(float x, float y)
{
	this->x = x;
	this->y = y;
}

Vector2 Vector2::operator/(float a)
{
	return Vector2(x / a, y / a);
}

void Vector2::operator/=(float a)
{
	x /= a;
	y /= a;
}

Vector2 Vector2::operator*(float a)
{
	return Vector2(x * a, y * a);
}

void Vector2::operator*=(float a)
{
	x *= a;
	y *= a;
}

Vector2 Vector2::operator*(Vector2 v)
{
	return Vector2(x * v.x, y * v.y);
}

void Vector2::operator*=(Vector2 v)
{
	x *= v.x;
	y *= v.y;
}

Vector2 Vector2::operator-(Vector2 v)
{
	return Vector2(x - v.x, y - v.y);
}

void Vector2::operator-=(Vector2 v)
{
	x -= v.x;
	y -= v.y;
}

Vector2 Vector2::operator+(Vector2 v)
{
	return Vector2(x + v.x, y + v.y);
}

void Vector2::operator+=(Vector2 v)
{
	x += v.x;
	y += v.y;
}

void Vector2::Normalise()
{
	float magnitude = Magnitude();
	x *= magnitude;
	y *= magnitude;
}

// This is built with inspiration from Quake III's Fast Inverse Square Root solution
// https://www.youtube.com/watch?v=p8u_k2LIZyo
float Vector2::Magnitude()
{
	float sum = (x * x) + (y * y);
	float halfSum = sum / 2.0f;
	long i = *(long*)&sum;
	i = 0x5f3759df - (i >> 1);
	sum = *(float*)&i;
	sum *= 1.5f - (halfSum * sum * sum);
	return sum;
}

std::ostream& operator<<(std::ostream& os, Vector2 v)
{
	return os << "(" << v.x << ", " << v.y << ")";
}