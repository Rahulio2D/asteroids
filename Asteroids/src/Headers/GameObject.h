#pragma once

#include "Transform.h"

class GameObject
{
public:
  GameObject();
  virtual ~GameObject();
  virtual void Initialise() = 0;
  virtual void Update() = 0;
  virtual void Render() = 0;

  Transform* transform;
};