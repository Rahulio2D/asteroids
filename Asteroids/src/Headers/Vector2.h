#pragma once
#include <ostream>

struct Vector2
{
  float x;
  float y;

  Vector2();
  Vector2(float x, float y);

  Vector2 operator/(float a);
  void operator/=(float a);

  Vector2 operator*(float a);
  void operator*=(float a);

  Vector2 operator*(Vector2 v);
  void operator*=(Vector2 v);

  Vector2 operator-(Vector2 v);
  void operator-=(Vector2 v);

  Vector2 operator+(Vector2 v);
  void operator+=(Vector2 v);

  void Normalise();
  float Magnitude();

  friend std::ostream& operator<<(std::ostream& os, Vector2 v);
};