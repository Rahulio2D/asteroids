#pragma once
#include <ostream>

struct Point
{
  int x;
  int y;

  Point();
  Point(float x, float y);

  friend std::ostream& operator<<(std::ostream& os, Point v);
};