#pragma once

#include "Transform.h"
#include "Vector2.h"

class BoxCollider
{
public:
  BoxCollider(Transform* transform, Vector2 offset = Vector2(), Vector2 size = Vector2(1, 1));

  Vector2 GetPosition();
  Vector2 GetSize();
  bool HasCollided(BoxCollider* otherCollider);

  Vector2 offset;
  Vector2 size;

private:
  Transform* transform;
};