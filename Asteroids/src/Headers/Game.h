#pragma once

#include "SDL.h"
#include "SDL_image.h"
#include "Camera.h"
#include "Input.h"
#include "Texture.h"
#include "GameObject.h"
#include "GameObjects/Player.h"
#include "GameObjects/AsteroidSpawner.h"
#include "GameObjects/Background.h"

class Game
{
public:
	Game(const char* title, int xPos, int yPos, int width, int height, bool fullscreen);
	~Game();

	void Initialise();
	void Update();
	void HandleEvents();
	void Render();
	bool IsRunning() { return isRunning; }
	static void GameOver();

	static SDL_Renderer* GET_RENDERER() { return renderer; }
	static Texture* GET_MAIN_TEXTURE() { return mainTexture; }
	static unsigned int WIDTH;
	static unsigned int HEIGHT;

private:
	int InitialiseSDL(const char* title, int xPos, int yPos, int width, int height, bool fullscreen);
	int CreateWindow(const char* title, int xPos, int yPos, int width, int height, bool fullscreen);
	int CreateRenderer();

	bool isRunning = false;
	static bool isMainMenu;
	SDL_Window* window;
	static SDL_Renderer* renderer;
	Camera* camera;
	Input* input;
	static Texture* mainTexture;
	Player* player;
	AsteroidSpawner* asteroidSpawner;
	Background* background;
};