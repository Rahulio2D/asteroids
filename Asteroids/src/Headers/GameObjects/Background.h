#pragma once

#include "GameObject.h"
#include "TextureRenderer.h"

class Background : public GameObject
{
public:
  Background();
  ~Background();
  void Initialise();
  void Update();
  void Render();

  void SetIsMainMenu(bool isMainMenu);

private:
  bool isMainMenu = true;
  TextureRenderer* textureRenderer;
};