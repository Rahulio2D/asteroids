#pragma once

#include "GameObject.h"
#include "Asteroid.h"
#include "Player.h"
#include <vector>

class AsteroidSpawner : public GameObject {
public:
  AsteroidSpawner(Player* player);
  ~AsteroidSpawner();
  void Initialise();
  void Update();
  void Render();
  void StopAndClear();

private:
  std::vector<Asteroid*> asteroids;
  Player* player;
  Vector2 screenSizeInUnits;

  float maxTimeToSpawnAsteroid = 5.3;
  float timeToNextAsteroidSpawn = 5.3;

  Asteroid* SpawnAsteroid();
};