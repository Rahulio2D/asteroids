#pragma once

#include "GameObject.h"
#include "TextureRenderer.h"
#include "BoxCollider.h"

class Bullet : public GameObject
{
public:
  Bullet(Vector2 spawnPosition, float bulletAngle);
  ~Bullet();
  void Initialise();
  void Update();
  void Render();

  BoxCollider* GetBoxCollider() { return boxCollider; }
  bool IsDestroyed() { return timeAlive >= timeTillDeath; }

private:
  TextureRenderer* textureRenderer;
  BoxCollider* boxCollider;

  Vector2 moveDirection;
  float moveSpeed = 17;

  float timeTillDeath = 7;
  float timeAlive = 0;
};