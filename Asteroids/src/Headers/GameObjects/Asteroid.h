#pragma once

#include "GameObject.h"
#include "TextureRenderer.h"
#include "BoxCollider.h"

class Asteroid : public GameObject
{
public:
  Asteroid(Vector2 spawnPosition, Vector2 playerPosition, bool isMiniAsteroid = false);
  ~Asteroid();
  void Initialise();
  void Update();
  void Render();
  void Destroy();

  BoxCollider* GetBoxCollider() { return boxCollider; }
  bool IsDestroyed() { return isDestroyed; }
  bool IsMiniAsteroid() { return isMiniAsteroid; }

private:
  TextureRenderer* textureRenderer;
  BoxCollider* boxCollider;

  float moveSpeed;
  Vector2 moveDirection;
  float rotationSpeed;
  bool isDestroyed = false;
  bool isMiniAsteroid;

  void UpdatePosition();
  void Rotate();
  void KeepOnScreen();

  Vector2 screenSizeInUnits;
};