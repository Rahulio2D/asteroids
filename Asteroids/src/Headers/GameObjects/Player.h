#pragma once

#include "GameObject.h"
#include "TextureRenderer.h"
#include "BoxCollider.h"
#include "Bullet.h"
#include "Asteroid.h"
#include <vector>

class Player : public GameObject
{
public:
  Player();
  ~Player();
  void Initialise();
  void Update();
  void Render();

  BoxCollider* GetBoxCollider() { return boxCollider; }
  void CheckCollisionWithAsteroid(Asteroid* asteroid);

private:
  TextureRenderer* textureRenderer;
  BoxCollider* boxCollider;
  std::vector<Bullet*> bullets;

  float currentAccelerationSpeed = 0;
  float maxAccelerationSpeed = 5.5;
  float maxFloatingSpeed = 1.3;
  float rotationSpeed = 190;
  
  float timeToMaxAcceleration = 1.3;
  float currentAccelerationAngle = 0;
  bool hasStartedMoving = false;

  void Accelerate();
  void Decelerate();
  void Turn(int direction);
  void UpdatePosition();
  void KeepOnScreen();
  void UpdateBullets();
  void ShootBullet();

  Vector2 screenSizeInUnits;
};