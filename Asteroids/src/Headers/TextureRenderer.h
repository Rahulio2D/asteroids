#pragma once

#include "Texture.h"
#include "Transform.h"
#include "SDL.h"

class TextureRenderer
{
public:
  TextureRenderer(Texture* texture, Transform* transform, SDL_Rect sourceRect);
  ~TextureRenderer();
  void Render();
  void SetSourceRect(SDL_Rect sourceRect);

private:
  Transform* transform;
  Texture* texture;
  SDL_Rect sourceRect;
  SDL_Rect destinationRect;

  void CalculateDestinationRect();
};