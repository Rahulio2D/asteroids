#pragma once

#include "SDL.h"
#include "Vector2.h"
#include "Point.h"

struct Camera
{
  Camera(float orthographicSize);
  void SetOrthographicSize(float orthographicSize);

  static float GetOrthographicSize() { return orthographicSize; }
  static float GetUnitSize() { return unitSize; }

  static SDL_Rect GetDestinationRect(Vector2 position, Vector2 size);

private:
  static Point center;
  static float orthographicSize;
  static float unitSize;
};