#pragma once

#include "Vector2.h"

struct Transform 
{
  Vector2 position;
  Vector2 size;
  float rotation;

  Transform();
};