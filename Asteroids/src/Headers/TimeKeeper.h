#pragma once

#include <SDL.h>

class TimeKeeper
{
public:
  static void CalculateDeltaTime();
  static double GetDeltaTime() { return deltaTime; }

private:
  static double deltaTime;

  static uint64_t ticksPreviousFrame;
  static uint64_t ticksThisFrame;
};