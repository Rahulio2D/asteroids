#pragma once

#include "SDL_image.h"

class Texture
{
public:
  Texture(const char* texturePath);
  ~Texture();

  SDL_Texture* GetTexture() { return texture; }

private:
  SDL_Texture* texture;
  SDL_Surface* LoadSurface(const char* texturePath);
  void CreateTexture(SDL_Surface* surface, const char* texturePath);
};