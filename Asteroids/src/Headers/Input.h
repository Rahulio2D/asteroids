#pragma once

#include "SDL.h"
#include <vector>

class Input {
public:
	Input();
	~Input();

	void ClearInput();
	void AddKeyPressed(SDL_Keycode key);
	void AddKeyReleased(SDL_Keycode key);

	static bool GetKey(SDL_Keycode key);			// Key is held down
	static bool GetKeyDown(SDL_Keycode key);	// Key was pressed this frame
	static bool GetKeyUp(SDL_Keycode key);		// Key was released this frame

private:
	static std::vector<SDL_Keycode> keysCurrentlyHeld;
	static std::vector<SDL_Keycode> keysPressedThisFrame;
	static std::vector<SDL_Keycode> keysReleasedThisFrame;

	static int AddUniqueKey(std::vector<SDL_Keycode>* keysVector, SDL_Keycode key);
	static bool FoundKey(std::vector<SDL_Keycode>* keysVector, SDL_Keycode key);
};